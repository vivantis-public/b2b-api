# Vivantis B2B API

## API Documentation

### URL
The base address from which all endpoints are derived: `http://b2b.vivantis.cz/api/v2`.

Only specific endpoints without a base address for requests are listed in the documentation. The base address needs to be added during implementation.

> ### Testing
> For test mode it is necessary to send a header `X-Vivantis-Test = 1` in the requests.
>
> In the test Mode, you can only work with products that have been added during testing. Products added in production mode are not available and cannot be affected.
>
> Test orders are created during testing. Only products added during testing are moved from the cart to test orders.


### Authorization
#### Endpoint: `HEAD /auth/authorize`

For authorization is needed the customer ID and code assigned to the customer on request.

Both the ID and the code are sent in the header `X-Vivantis-Auth` in format `ID$code`.

The response contains a header `X-Vivantis-Token`, that needs to be included in each subsequent request.


### Token validation
#### Endpoint: `HEAD /auth/verify`

The validity of the token is time limited. Therefore, it is recommended to verify its validity before each communication with the API.

Verification is done by sending a token `X-Vivantis-Token` in the header.

In the case of validity, the response is a header `X-Vivantis-Token` containing a new token (old is invalidated), or HTTP code 422 and a header `X-Vivantis-Message` containing a reason of unsuccessful verification.

If authentication is unsuccessful, you must log in again to obtain a valid token.


### Shopping cart

> **Testing**
>
> For testing, you need to add a header `X-Vivantis-Test = 1` to the requests.
>
> Products added in production mode are not available during testing.

##### Endpoint: `GET /shopping-cart`
Returns shopping cart status information.

**JSON of the shopping cart**
```json
{
    "shoppingCart": {
        "priceLockedTo": "",
        "products": []
    }
}
```
<dl>
    <dt>priceLockedTo:</dt>
    <dd>string<br> date and time in the format <code>YYYY-MM-DD HH:MM:SS</code><br>an empty value indicates that prices are not locked<br>price lockout is required to create an order</dd>
    <dt>products:</dt>
    <dd>array<br>field with products in shopping cart</dd>
</dl>

**JSON of the product in shopping cart**
```json
{
    "productCode": "xYZ128",
    "priceWithoutVat": 1.0,
    "priceWithVat": 1.0,
    "quantity": 1,
    "inStock": 1,
    "description": "",
    "toOrder": true
}
```
<dl>
    <dt>productCode</dt>
    <dd>string<br>product code in the Vivantis B2B system</dd>
    <dt>priceWithoutVat</dt>
    <dd>float<br>price without VAT available at the moment of insertion into the cart<br>when prices are locked, they are updated</dd>
    <dt>priceWithVat</dt>
    <dd>float<br>price with VAT available at the moment of insertion into the cart<<br>when prices are locked, they are updated</dd>
    <dt>quantity</dt>
    <dd>integer<br>quantity of product in the shopping cart</dd>
    <dt>inStock</dt>
    <dd>integer<br>quantity of product in stock</dd>
    <dt>description</dt>
    <dd>string<br>description of product<br>is available to B2B operators</dd>
    <dt>toOrder</dt>
    <dd>boolean<br>the value determines whether the product is to be moved (into the order) or left in the shopping cart when creating the order</dd>
</dl>

##### Endpoint: `POST /shopping-cart`
Adds products to the shopping cart. It is possible to add one or more products, but in both cases it must be a field containing products.

Returned data is JSON of the shopping cart.

**JSON of the added product**
```json
[
    {
        "productCode": "xYZ128",
        "quantity": 1,
        "description": "",
        "toOrder": true
    }
]
```
<dl>
    <dt>productCode</dt>
    <dd>string<br>product code in the Vivantis B2B system</dd>
    <dt>quantity</dt>
    <dd>integer<br>optional<br>quantity of the product added to shopping cart<br>default value: 1</dd>
    <dt>description</dt>
    <dd>string<br>optional<br>description of product</dd>
    <dt>toOrder</dt>
    <dd>boolean<br>optional<br>setting the transfer to order<br>default value: TRUE</dd>
</dl>

##### Endpoint: `PUT /shopping-cart`
Edit product values in shopping cart. The JSON structure is the same as for `POST`.

Returned data is JSON of the shopping cart.

##### Endpoint: `DELETE /shopping-cart`
Deletes all products from the shopping cart.

Returns HTTP code `204`.

#### Endpoint: `DELETE /shopping-cart/<code>`
Deletes the product from the shopping cart.

Returns HTTP code `204`.

`<code>` contains the Vivantis product code.

##### Endpoint: `HEAD /shopping-cart/lock-prices`

Locks product prices in cart.


### Addresses
#### Endpoint: `GET /addresses`
Returns a list of customer addresses.

**JSON of the list**
```json
[
    {
        "name": "Chrudim - warehouse",
        "default": false,
        "street": "Školní náměstí",
        "numberD": "14",
        "numberO": null,
        "city": "Chrudim",
        "zip": "53701",
        "countryCode": "CZE",
        "contactPerson": "Jan Novák",
        "phone": "+420 777444000",
        "email": "jan.novak@vivantis.cz"
    }
]
```
<dl>
    <dt>id</dt>
    <dd>integer<br>ID of the address in Vivantis B2B system</dd>
    <dt>name</dt>
    <dd>string<br>unique name of the address</dd>
    <dt>default</dt>
    <dd>boolean<br>default for selecting in e-shop</dd>
    <dt>street</dt>
    <dd>string<br>street name</dd>
    <dt>numberD</dt>
    <dd>string<br>house number</dd>
    <dt>numberO</dt>
    <dd>string<br>optional<br>second number</dd>
    <dt>zip</dt>
    <dd>string<br>postal code</dd>
    <dt>countryCode</dt>
    <dd>string<br>ISO 3 country code</dd>
    <dt>contactPerson</dt>
    <dd>string<br>optional<br>contact person</dd>
    <dt>phone</dt>
    <dd>string<br>contact phone number (area code separated by a space "<code> </code>")</dd>
    <dt>email</dt>
    <dd>string<br>contact email</dd>
</dl>

##### Endpoint: `POST /addresses`
Creates a new delivery address.

The returned data is the address in JSON format.

**JSON for creating an address**
```json
{
    "name": "Chrudim - warehouse",
    "default": false,
    "street": "Školní náměstí",
    "numberD": "14",
    "numberO": null,
    "city": "Chrudim",
    "zip": "53701",
    "countryCode": "CZE",
    "contactPerson": "Jan Novák",
    "phone": "+420 777444000",
    "email": "jan.novak@vivantis.cz"
}
```
<dl>
    <dt>name</dt>
    <dd>string<br>unique<br>name of the address</dd>
    <dt>default</dt>
    <dd>boolean<br>optional<br>sets the address as default<br>default value: FALSE</dd>
    <dt>street</dt>
    <dd>string<br>street name</dd>
    <dt>numberD</dt>
    <dd>string<br>optional<br>house number</dd>
    <dt>numberO</dt>
    <dd>string<br>optional<br>orientation number</dd>
    <dt>city</dt>
    <dd>string<br>city</dd>
    <dt>zip</dt>
    <dd>string<br>postal code</dd>
    <dt>countryCode</dt>
    <dd>string<br>ISO 3 country code</dd>
    <dt>contactPerson</dt>
    <dd>string<br>optional<br>contact person</dd>
    <dt>phone</dt>
    <dd>string<br>contact phone number (area code separated by a space "<code> </code>")</dd>
    <dt>email</dt>
    <dd>string<br>contact email</dd>
</dl>

### Orders
#### Endpoint: `POST /orders/<id>`
Creates an order from items in the cart marked `toOrder = true`.

To create it, you need to send data with the ID of the address to which the order should be sent.

Optionally, you can also send the own description or order's number (`description`, `customerOrderNumber`).

If an order already exists (for the same address) and is open (`state =" OPEN "`), then no new order is created, but the existing one is used. At one time, just one order can be open to a given address.

> **Testing**
>
> To create a test order, it is necessary to send a request with a header `X-Vivantis-Test = 1`.
> 
> In this case, the order will only be created from the products added during testing.

**JSON of the address**
```json
{
    "id": 9,
    "description": "Lorem ipsum dolor sit amet.",
    "customerOrderNumber": "1024/lipsum/2020"
}
```

##### Endpoint: `GET /orders[/<id>]`
Returns a list of all customer orders, or detail of one specific order.

The request is made without `/<id>` for the list. For the detail of the order, the order ID will replace `<id>`.

The list can be filtered by optional parameters `date_from` and `date_to` for `created` value. Values must have the format `YYYY-MM-DD`.

**JSON of the list**
```json
[
    {
        "id": 1,
        "created": "1900-01-01 00:00:00",
        "state": "OPEN",
        "description": "Lorem ipsum dolor sit amet.",
        "customerOrderNumber": "1024/lipsum/2020",
        "addingItems": TRUE,
        "countItems": 1,
        "items": []
    }
]
```
<dl>
    <dt>id</dt>
    <dd>integer<br>ID of the order in Vivantis B2B system</dd>
    <dt>created:</dt> 
    <dd>string<br>date and time of order creation in format <code>YYYY-MM-DD HH:MM:SS</code></dd>
    <dt>state</dt>
    <dd>string<br>describes the status of the order</dd>
    <dt>description</dt>
    <dd>string|null<br>customer's description for order</dd>
    <dt>customerOrderNumber</dt>
    <dd>string|null<br>customer number of order</dd>
    <dt>addingItems</dt>
    <dd>boolean<br>indicates the possibility to add more items to the order</dd>
    <dt>countItems</dt>
    <dd>integer<br>indicates the number of items in the order</dd>
    <dt>items</dt>
    <dd>array<br>products in order<br>the list of products is not sent in the summary</dd>
</dl>

**JSON of the detail**
```json
{
    "order": {
        "id": 1,
        "inserted": "1900-01-01 00:00:00",
        "state": "OPEN",
        "description": "Lorem ipsum dolor sit amet.",
        "customerOrderNumber": "1024/lipsum/2020",
        "addingItems": TRUE,
        "countItems": 1,
        "items": [
            {
                "productCode": "",
                "description": "",
                "quantity": 1,
                "priceWithoutVat": 1.0,
                "priceWithVat": 1.0,
                "inserted": "1900-01-01 00:00:00",
                "state": "ORDERED"
            }
        ]
    }
}
```

The `state` can take the following statuses on the order:
<dl>
    <dt>OPEN</dt>
    <dd>open<br>it is still possible to add more products from the shopping cart to the order<br>the products are added automatically if the order is to the same address as the one selected when creating the order from the shopping cart</dd>
    <dt>LOCK</dt>
    <dd>preparation for expedition</dd>
    <dt>SHIPPING</dt>
    <dd>in the expedition</dd>
    <dt>PREPARED</dt>
    <dd>prepared for shipping</dd>
    <dt>SHIPPED</dt>
    <dd>forwarded to the carrier</dd>
    <dt>CLOSED</dt>
    <dd>closed</dd>
</dl>

The `state` can take the following statuses on the item:
<dl>
    <dt>ORDERED</dt>
    <dd>the product has been ordered/prepared for shopping</dd>
    <dt>EXPEDITED</dt>
    <dd>the product was shipped</dd>
    <dt>CANCELED</dt>
    <dd>the product was canceled</dd>
    <dt>RELOCATED</dt>
    <dd>the product comes from an earlier order for which it was not available in the required quantity</dd>
    <dt>DELAYED</dt>
    <dd>expedition of the product will be delayed</dd>
    <dt>SPLITTED</dt>
    <dd>the required amount of product was splitted because the product was not available in the required quantity</dd>
</dl>

#### Endpoint: `GET /order/<id>.<extension>`
Returns the invoice for order `<id>`. According to `<extension>` you can choose the format of the invoice.
<dl>
    <dt>pdf</dt>
    <dd>Invoice as PDF</dd>
    <dt>xml</dt>
    <dd>Invoice as XML</dd>
    <dt>xlsx</dt>
    <dd>Invoice as Excel</dd>
</dl>

##### Endpoint: `HEAD /orders/lock`
Locks open order. It is not possible to add more products to the locked order.

When ordering other products, a new unlocked order is created.


### Error statuses
When an error occurs on the server, JSON is returned with the error information and also the corresponding HTTP code is returned.

**JSON errors**
```json
{
    "code": 500,
    "status": "",
    "message": "",
    "errors": []
}
```
<dl>
    <dt>code</dt>
    <dd>HTTP kód</dd>
    <dt>status</dt>
    <dd>error status</dd>
    <dt>message</dt>
    <dd>error message describing the problem</dd>
    <dt>errors</dt>
    <dd>field containing additional error information</dd>
</dl>

##### 404 code
The requested data could not be found. This may occur when product or order details are requested.

##### 422 code
The data sent has the correct syntax but does not match. Typically, this may be a bad login, a bad token, or an attempt to add or edit an invalid product.

##### 423 code
It may appear when requesting an invoice for an order. The invoice may not yet exist at the time of the request. 

##### 500 code
An error occurred while processing the request.


## Documentation of the library of B2BApi
The VivantisApi library solves the program part for working with individual endpoints, sent data and authorizations and is applicable since PHP 8.1.

Required implementations of  [PSR-18 HTTP Client](https://www.php-fig.org/psr/psr-18/) a [PSR-16 Simple Cache](https://www.php-fig.org/psr/psr-16/).

### Installation
The library is installed via Composer.

```json
{
    "repositories": [
        {
            "type": "git",
            "url": "https://gitlab.com/vivantis-public/b2b-api.git"
        }
    ],
    "require": {
        "vivantis/b2b-api": "~3.0"
    }
}
```


### Clients
Clients provide communication with Vivantis API. For their functionality clients require instance of the class `Vivantis\B2BApi\Service\TokenService`.

For test mode, you need to call the `setTestMode ()` method, which tells the client that the following requests will be tested and will also be flagged.


### Entities
Entities represent objects sent in JSON through the API. It is possible to get their data as `array`.

It is created using either the `Vivantis\B2BApi\Entity\EntityFactory` class or the `new *Entity` call.

Collections are instances of a class `Vivantis\B2BApi\Collection\Collection`.


### Services
Services provide additional processes for authorization and token handling.


### TokenService class
It provides storing and retrieving tokens. For its functionality it requires a class instance implementing `Psr\SimpleCache\CacheInterface`.


### ApiFacade class
It represents a facade consisting of specific operations into processes that may be needed for implementation.

The class requires login information (`clientId` and `clientSecretCode`) and token storage.

The `ApiFacade::init()` method provides token validation and the new login when the token is invalid.

The ApiFacade class also makes instances of individual clients accessible via `ApiFacade::$shoppingCartClient`, `ApiFacade::$addressClient` and `ApiFacade::$orderClient`.

For testing mode just call `ApiFacade::setTestMode()` and the class tells all clients that the following requests will be in testing mode.


### Mediation of error statuses
If the server responds to the request with an error status, it is returned `Vivantis\B2BApi\Exception\RequestException`, which contains `$message`, `$code` and `$errors`.
