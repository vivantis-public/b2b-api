# Vivantis B2B API

## Dokumentace API

### URL
Základní adresa, od níž se odvozují všechny endpointy je `https://b2b.vivantis.cz/api/v2`.

Kvůli lepší přehlednosti jsou v dokumentaci u requestů vypisovány pouze konkrétní endpointy bez základní adresy. Tu je při implementaci nutné doplnit.

> ### Testování
> Pro testovací režim je nutné v requestech posílat hlavičku `X-Vivantis-Test = 1`.
>
> V testovacím režimu lze v košíku pracovat pouze s produkty, které byli v během testování přidány. Produkty přidáné v ostrém režimu nejsou dostupné a nelze je ovlivňovat.</p>
>
> Během testování jsou vytvářeny testovací objednávky. Do nich se z košíku přesunují pouze produkty přidané během testování.


### Přihlášení
#### Endpoint: `HEAD /auth/authorize`
 
Přihlášení probíhá pomocí zákaznického identifikátoru a kódu, které jsou zákazníkovy přiřazeny na vyžádání.

Identifikátor i kód se odesílají v hlavičce `X-Vivantis-Auth` ve formátu `ID$code`.

Odpověď obsahuje hlavičku `X-Vivantis-Token`, kterou je třeba vložit do každého následujícího requestu.


### Ověření platnosti tokenu
#### Endpoint: `HEAD /auth/verify`

Platnost tokenu je časově omezena. Proto je doporučeno si před každým začátkem komunikace s API ověřit jeho platnost. 

Ověření se provádí odeslaním tokenu v hlavičce `X-Vivantis-Token`.

Odpovědí je v případě platnosti hlavička `X-Vivantis-Token` obsahující nový token (starý je zneplatněn), nebo HTTP kód 422 a hlavička `X-Vivantis-Message` obsahující důvod neúspěšného ověření.

V případě neúspěšného ověření je pro získání platného tokenu nutné znova se přihlásit.

### Košík

> **Testování**
> 
> Pro testování je nutné do requestů přidávat hlavičku `X-Vivantis-Test = 1`.
> 
> Během testování nejsou k dispozici produkty přidané v ostrém režimu.

##### Endpoint: `GET /shopping-cart`
Vrací informaci o stavu košíku.

**JSON košíku**
```json
{
    "shoppingCart": {
        "priceLockedTo": "",
        "products": []
    }
}
```
<dl>
    <dt>priceLockedTo:</dt> 
    <dd>string<br>datum a čas ve formátu <code>YYYY-MM-DD HH:MM:SS</code><br>prázdná hodnota značí, že ceny nejsou uzamčeny<br>uzamčení cen je nutné pro vytvoření objednávky</dd>
    <dt>products:</dt> 
    <dd>array<br>pole s produkty v košíku</dd>
</dl>

**JSON produktu v košíku**
```json
{
    "productCode": "xYZ128",
    "priceWithoutVat": 1.0,
    "priceWithVat": 1.0,
    "quantity": 1,
    "inStock": 1,
    "description": "",
    "toOrder": true
}
```
<dl>
    <dt>productCode</dt>
    <dd>string<br>kód produktu v systému Vivantis B2B</dd>
    <dt>priceWithoutVat</dt>
    <dd>float<br>cena produktu bez DPH dostupná ve chvíli vložení do košíku<br>při uzamčení cen dojde k jejich aktualizaci</dd>
    <dt>priceWithVat</dt>
    <dd>float<br>cena produktu s DPH dostupná ve chvíli vložení do košíku<br>při uzamčení cen dojde k jejich aktualizaci</dd>
    <dt>quantity</dt>
    <dd>integer<br>množství produktu vložené v košíku</dd>
    <dt>inStock</dt>
    <dd>integer<br>dostupné množství produktu na skladě</dd>
    <dt>description</dt>
    <dd>string<br>zákaznická poznámka k produktu<br>je dostupná obsluze B2B</dd>
    <dt>toOrder</dt>
    <dd>boolean<br>hodnota určuje, zdali se má produkt při vytváření objednávky do ní přesunout, či v košíku zůstat</dd>
</dl>

##### Endpoint: `POST /shopping-cart`
Přidává produktů do košíku. Je možné přidat jeden nebo více produktů, v obou případech se ale musí jednat o pole, obsahující produkt/y.

Vrácená data jsou JSON košíku.

**JSON přidávaného produktu**
```json
[
    {
        "productCode": "xYZ128",
        "quantity": 1,
        "description": "",
        "toOrder": true
    }
]
```
<dl>
    <dt>productCode</dt>
    <dd>string<br>kód produktu v systému Vivantis</dd>
    <dt>quantity</dt>
    <dd>integer<br>nepovinné<br>množství přidávané do košíku<br>výchozí hodnota: 1</dd>
    <dt>description</dt>
    <dd>string<br>nepovinné<br>zákaznická poznámka k produktu</dd>
    <dt>toOrder</dt>
    <dd>boolean<br>nepovinné<br>nastavení převodu do objednávky<br>výchozí hodnota: TRUE</dd>
</dl>

##### Endpoint: `PUT /shopping-cart`
Upraví hodnoty produktu v košíku. Struktura JSONu je stejná jako pro `POST`.

Vrácená data jsou JSON košíku.

##### Endpoint: `DELETE /shopping-cart`
Smaže všechny produkty z košíku.

Vrací HTTP kód `204`.

##### Endpoint: `DELETE /shopping-cart/<code>`
Smaže produkt z košíku. 

Vrací HTTP kód `204`.

`<code>` obsahuje kód produktu v systému Vivantis.

##### Endpoint: `HEAD /shopping-cart/lock-prices`
Uzamkne ceny produktů v košíku.


### Adresy
##### Endpoint: `GET /addresses`

Vrací seznam zákaznických adres.

**JSON seznamu**
```json
[
    {
        "name": "Chrudim - sklad",
        "default": false,
        "street": "Školní náměstí",
        "numberD": "14",
        "numberO": null,
        "city": "Chrudim",
        "zip": "53701",
        "countryCode": "CZE",
        "contactPerson": "Jan Novák",
        "phone": "+420 777444000",
        "email": "jan.novak@vivantis.cz"
    }
]
```
<dl>
    <dt>id</dt>
    <dd>integer<br>ID adresy v systému Vivantis</dd>
    <dt>name</dt>
    <dd>string<br>unikátní<br>název adresy</dd>
    <dt>default</dt>
    <dd>boolean<br>výchozí pro výběr v e-shopu</dd>
    <dt>street</dt>
    <dd>string<br>název ulice</dd>
    <dt>numberD</dt>
    <dd>string<br>číslo popisné</dd>
    <dt>numberO</dt>
    <dd>string<br>nepovinné<br>číslo orientační</dd>
    <dt>zip</dt>
    <dd>string<br>poštovní směrovací číslo</dd>
    <dt>countryCode</dt>
    <dd>string<br>ISO 3 kód státu</dd>
    <dt>contactPerson</dt>
    <dd>string<br>nepovinné<br>kontaktní osoba</dd>
    <dt>phone</dt>
    <dd>string<br>kontaktní telefon (předvolba oddělena mezerou "<code> </code>")</dd>
    <dt>email</dt>
    <dd>string<br>kontaktní e-mail</dd>
</dl>

##### Endpoint: `POST /addresses`
Vytváří novou dodací adresu.

Vrácená data jsou JSON adresy.

**JSON pro vytvoření adresy**
```json
{
    "name": "Chrudim - sklad",
    "default": false,
    "street": "Školní náměstí",
    "numberD": "14",
    "numberO": null,
    "city": "Chrudim",
    "zip": "53701",
    "countryCode": "CZE",
    "contactPerson": "Jan Novák",
    "phone": "+420 777444000",
    "email": "jan.novak@vivantis.cz"
}
```
<dl>
    <dt>name</dt>
    <dd>string<br>unikátní<br>název adresy</dd>
    <dt>default</dt>
    <dd>boolean<br>nepovinné<br>nastaví adresu jako výchozí<br>výchozí hodnota: FALSE</dd>
    <dt>street</dt>
    <dd>string<br>název ulice</dd>
    <dt>numberD</dt>
    <dd>string<br>nepovinné<br>číslo popisné</dd>
    <dt>numberO</dt>
    <dd>string<br>nepovinné<br>číslo orientační</dd>
    <dt>city</dt>
    <dd>string<br>Město</dd>
    <dt>zip</dt>
    <dd>string<br>poštovní směrovací číslo</dd>
    <dt>countryCode</dt>
    <dd>string<br>ISO 3 kód státu</dd>
    <dt>contactPerson</dt>
    <dd>string<br>nepovinné<br>kontaktní osoba</dd>
    <dt>phone</dt>
    <dd>string<br>kontaktní telefon (předvolba oddělena mezerou "<code> </code>")</dd>
    <dt>email</dt>
    <dd>string<br>kontaktní e-mail</dd>
</dl>


### Objednávky
##### Endpoint: `POST /orders/<id>`

Vytváří z položek v košíku označených `toOrder = true` objednávku.

Pro vytvoření je potřeba poslat data s ID adresy, na kterou má objednávka být zaslána.

Volitelně lze nastavit i zákaznický popisek `description` nebo číslo objednávky `customerOrderNumber`.

Pokud pro danou adresu již objednávka existuje a je otevřená (`state = "OPEN"`), pak se nevytváří nová objednávka, ale použije se stávající. V jednu chvíli může být na danou adresu otevřená právě jedna objednávka.

> **Testování**
>
> Pro vytvoření testovací objednávky je nutné poslat request s hlavičkou `X-Vivantis-Test = 1`.
> 
> V takovém případě se vytvoří objednávka pouze z produktů přidaných během testování.

**JSON adresy**
```json
{
    "id": 9,
    "description": "Lorem ipsum dolor sit amet.",
    "customerOrderNumber": "1024/lipsum/2020"
}
```

##### Endpoint `GET  /orders[/<id>]`
Vrátí seznam všech objednávek zákazníka, nebo detail jedné konkrétní. 

Pro seznam se provádí request bez `/<id>`. Pro detail objednávky se `<id>` nahradí ID objednávky.

Seznam lze filtrovat nepovinnými parametry `date_from` a `date_to` pro hodnotu `created`. Hodnoty musí mít formát `YYYY-MM-DD`.

**JSON seznamu**
```json
[
    {
        "id": 1,
        "created": "1900-01-01 00:00:00",
        "state": "OPEN",
        "description": "Lorem ipsum dolor sit amet.",
        "customerOrderNumber": "1024/lipsum/2020",
        "addingItems": true,
        "countItems": 1,
        "items": []
    }
]
```
<dl>
    <dt>id</dt>
    <dd>integer<br>ID objednávky v systému Vivantis</dd>
    <dt>created:</dt> 
    <dd>string<br>datum a čas vytvoření objednávky ve formátu <code>YYYY-MM-DD HH:MM:SS</code></dd>
    <dt>state</dt>
    <dd>string<br>popisuje stav, ve kterém se objednávka nachází</dd>
    <dt>description</dt>
    <dd>string|null<br>zákaznická poznámka k objednávce</dd>
    <dt>customerOrderNumber</dt>
    <dd>string|null<br>zákaznické číslo objednávky</dd>
    <dt>addingItems</dt>
    <dd>boolean<br>signalizuje možnosti přidávat další položky do objednávky</dd>
    <dt>countItems</dt>
    <dd>integer<br>udává počet položek v objednávce</dd>
    <dt>items</dt>
    <dd>array<br>produkty v objednávce<br>v přehledu se seznam produktů neposílá</dd>
</dl>

**JSON detailu**
```json
{
    "order": {
        "id": 1,
        "created": "1900-01-01 00:00:00",
        "state": "OPEN",
        "description": "Lorem ipsum dolor sit amet.",
        "customerOrderNumber": "1024/lipsum/2020",
        "addingItems": true,
        "countItems": 1,
        "items": [
            {
                "productCode": "",
                "description": "",
                "quantity": 1,
                "priceWithoutVat": 1.0,
                "priceWithVat": 1.0,
                "inserted": "1900-01-01 00:00:00",
                "state": "ORDERED"
            }
        ]
    }
}
```

Na objednávce může `state` nabývat těchto stavů:
<dl>
    <dt>OPEN</dt>
    <dd>otevřená<br>do objednávky je stále možné přidávat další produkty z košíku<br>produkty se přidávají automaticky, pokud je objednávka na stejnou adresu, jaká byla zvolena při vytváření objednávky z košíku</dd>
    <dt>LOCK</dt>
    <dd>příprava pro expedici</dd>
    <dt>SHIPPING</dt>
    <dd>v expedici</dd>
    <dt>PREPARED</dt>
    <dd>zkompletována</dd>
    <dt>SHIPPED</dt>
    <dd>předána dopravci</dd>
    <dt>CLOSED</dt>
    <dd>uzavřená</dd>
</dl>

Na položce může `state` nabývat těchto stavů:
<dl>
    <dt>ORDERED</dt>
    <dd>produkt by objednán/připraven pro expedici</dd>
    <dt>EXPEDITED</dt>
    <dd>produkt byl vyexpedován</dd>
    <dt>CANCELED</dt>
    <dd>produkt byl stornován</dd>
    <dt>RELOCATED</dt>
    <dd>produkt pochází z dřívější objednávky, pro kterou nebyl v požadovaném množství k dispozici</dd>
    <dt>DELAYED</dt>
    <dd>expedice produktu bude zpožděna</dd>
    <dt>SPLITTED</dt>
    <dd>požadované množství produktu bylo rozděleno, protože nebylo v plné výši k dispozici</dd>
</dl>

##### Endpoint: `GET /order/<id>.<extension>`
Vrátí fakturu pro objednávku `<id>`. Podle `<extension>` lze zvolit formát faktury.
<dl>
    <dt>pdf</dt>
    <dd>Faktura ve formátu PDF</dd>
    <dt>xml</dt>
    <dd>Faktura ve formátu XML</dd>
    <dt>xlsx</dt>
    <dd>Faktura ve formátu Excel</dd>
</dl>

##### Endpoint: `HEAD /orders/lock`
Uzamkne otevřenou objednávku. Do uzamčené objednávky není možno přidávat další produkty.

Při objednání dalších produktů se vytvoří nová odemčená objednávka.


### Chybové stavy
V případě, kdy dojde na serveru k chybě, je vrácen JSON s informací o dané chybě a HTTP kód chybě odpovídající.

**JSON chyby**
```json
{
    "code": 500,
    "status": "",
    "message": "",
    "errors": []
}
```
<dl>
    <dt>code</dt>
    <dd>HTTP kód</dd>
    <dt>status</dt>
    <dd>stav chyby</dd>
    <dt>message</dt>
    <dd>chybová hláška popisující problém</dd>
    <dt>errors</dt>
    <dd>pole obsahující dodatečné informace o chybě</dd>
</dl>

##### Kód 404
Požadovaná data nebyla nalezena. Může jít o vyžádání detailu produktu nebo objednávky.

##### Kód 422
Zaslaná data mají správnou syntaxi, ale neodpovídají. Typicky se může jednat o chybné přihlašovací údaje, nevalidní token či snaha přidat nebo upravit neplatný produkt.

##### Kód 423
Může se objevit při requestu na fakturu objednávky. Faktura ještě nemusí v době requestu existovat. 

##### Kód 500
Na serveru došlo při zpracování požadavku k chybě.


## Dokumentace knihovny B2BApi
Knihovna B2BApi řeší prográmatorskou část pro práci s jednotlivými endpointy, posílanými daty a autorizací a je použitelná od verze PHP 8.1.

Vyžaduje implementaci [PSR-18 HTTP Client](https://www.php-fig.org/psr/psr-18/) a [PSR-16 Simple Cache](https://www.php-fig.org/psr/psr-16/).

### Instalace
Knihovna se instaluje přes Composer.
```json
{
    "repositories": [
        {
            "type": "git",
            "url": "https://gitlab.com/vivantis-public/b2b-api.git"
        }
    ],
    "require": {
        "vivantis/b2b-api": "~3.0"
    }
}
```


### Klienti
Zajišťují komunikaci s API systému Vivantisu. Klienti pro svou funkcionalitu vyžadují instanci třídy `Vivantis\B2BApi\Service\TokenService`.

Pro testovací režim je nutné zavolat metodu `setTestMode()`, která klientovi řekne, že následující requesty budou testovací a ten je také tak označí.


### Entity
Představují objekty posílané v JSONu skrze API. Je možné získat jejich data jako `array`. 

Vytváří se buď pomocí třídy `Vivantis\B2BApi\Entity\EntityFactory` nebo klasicky přes volání `new *Entity`.

Kolekce jsou instancí třídy `Vivantis\B2BApi\Collection\Collection`.


### Služby
Zajišťují dodatečné procesy pro autorizaci a práci s tokeny.


### Třída TokenService
Zajišťuje ukládání a načítání tokenů. Pro svou funkcionalitu vyžaduje instanci třídy implementující `Psr\SimpleCache\CacheInterface`.


### Třída ApiFacade
Představuje fasádu skládající konkrétní úkony do procesů, které mohou být při implementaci potřebné.

Třída pro svou funkcionalitu vyžaduje přihlašovací údaje (`clientId` a `clientSecretCode`) a úložiště tokenů.

Metoda `ApiFacade::init()` obstarává ověření platnosti tokenu a pokud je neplatný i nové přihlášení.

Třída ApiFacade zpřístupňuje i instance jednotlivých klientů přes `ApiFacade::$shoppingCartClient`, `ApiFacade::$addressClient` a `ApiFacade::$orderClient`.

Pro testovací režim stačí zavolat `ApiFacade::setTestMode()` a třída všem klientům řekne, že následující requesty budou testovací.


### Zprostředkování chybových stavů
Pokud server na request odpoví chybým stavem, je vyhozena `Vivantis\B2BApi\Exception\RequestException`, která krom `$message` a `$code` obsahuje i `$errors`.
