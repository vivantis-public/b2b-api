# Vivantis B2B API

Documentation and SDK for using API to customer's administration a B2B of Vivantis.

## Documentation
 - [EN](./docs/en.md)
 - [CZ](./docs/cz.md)

[Examples](./examples.php) of using SDK for make order.
