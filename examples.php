<?php

use Psr\SimpleCache\CacheInterface;
use Vivantis\B2BApi\ApiFacade;
use Vivantis\B2BApi\Exception\RequestException;

require __DIR__ . '/vendor/autoload.php';

Tracy\Debugger::enable(Tracy\Debugger::Development, __DIR__);

try {
  // Settings
  $clientId = 1977853;
  $clientSecretCode = '8bb99d599d54f8ab6a87';

  // Some library implementing PSR-18, see https://www.php-fig.org/psr/psr-18/
  $httpClient = new GuzzleHttp\Client();

  // Some library implementing PSR-16, see https://www.php-fig.org/psr/psr-16/
  // This anonymous class is only for these examples, don't use is another else!
  $cache = new class implements CacheInterface
  {
    private array $cache = [];


    public function get($key, $default = null): mixed
    {
      return array_key_exists($key, $this->cache) ? $this->cache[$key] : $default;
    }


    public function set($key, $value, null|int|\DateInterval $ttl = null): bool
    {
      $this->cache[$key] = $value;

      return true;
    }


    public function delete($key): bool
    {
      unset($this->cache[$key]);

      return true;
    }


    public function clear(): bool
    {
      $this->cache = [];

      return true;
    }


    public function getMultiple(iterable $keys, mixed $default = null): iterable
    {
      $values = [];
      foreach ($keys as $key) {
        $values[$key] = $this->get($key);
      }

      return $values;
    }


    public function setMultiple(iterable $values, \DateInterval|int|null $ttl = null): bool
    {
      foreach ($values as $key => $value) {
        $this->set($key, $value);
      }

      return true;
    }


    public function deleteMultiple(iterable $keys): bool
    {
      foreach ($keys as $key) {
        $this->delete($key);
      }

      return true;
    }


    public function has(string $key): bool
    {
      return array_key_exists($key, $this->cache);
    }
  };

  $api = new ApiFacade($clientId, $clientSecretCode, $httpClient, $cache);

  $api->setTestMode();


  // List shopping cart products
  echo 'Products in shopping cart: ';
  $products = $api->getShoppingCartProducts();
  echo $products->count(), PHP_EOL;
  echo 'Products: ';
  dump($products->toArray());


  // Add product
  $productCode = 'hCF033';
  echo sprintf('Adding product "%s" to shopping cart: ', $productCode);
  $result = $api->addProductToShoppingCart($productCode);
  echo $result ? 'success' : 'failed', PHP_EOL;
  $productCode = 'kNU00602';
  echo sprintf('Adding product "%s" to shopping cart: ', $productCode);
  $result = $api->addProductToShoppingCart($productCode, 3, 'Client description.');
  echo $result ? 'success' : 'failed', PHP_EOL;
  echo 'Products in shopping cart now: ';
  $products = $api->getShoppingCartProducts();
  echo 'Products: ';
  dump($products->toArray());


  // Change quantity of product
  echo 'Changing quantity: ';
  $product = $products[0];
  $result = $api->changeProductQuantity($product->productCode, 10);
  echo $result ? 'success' : 'failed', PHP_EOL;
  echo 'Products: ';
  dump($api->getShoppingCartProducts()->toArray());


  // Change description of product
  echo 'Changing description: ';
  $result = $api->changeProductDescription($product->productCode, 'Example of change description.');
  echo $result ? 'success' : 'failed', PHP_EOL;
  echo 'Products: ';
  dump($api->getShoppingCartProducts()->toArray());


  // Change toOrder of product
  echo 'Changing toOrder: ';
  $result = $api->changeProductToOrder($product->productCode, false);
  echo $result ? 'success' : 'failed', PHP_EOL;
  echo 'Products: ';
  dump($api->getShoppingCartProducts()->toArray());


  // Lock prices in shopping cart for create order
  echo 'Locking prices: ';
  $result = $api->lockPrices();
  echo $result instanceof DateTimeInterface ? 'success, lock expires in ' . $result->format('d.m.Y H:i:s') : 'failed', PHP_EOL;
  echo 'Products: ';
  dump($api->getShoppingCartProducts()->toArray());


  // List addresses
  $addresses = $api->getAddresses();
  echo 'Addresses: ';
  dump($addresses->toArray());


  // List orders
  $orders = $api->getOrders();
  echo 'Count of orders: ', $orders->count(), PHP_EOL;


  // Create order
  $address = $addresses[0];
  echo 'Creating order: ';
  $result = $api->createOrder($address);
  echo 'New order: ';
  dump($result->toArray());
  echo 'Products in shopping cart: ';
  dump($api->getShoppingCartProducts()->toArray());


  // Remove products
  echo 'Removing all products: ';
  $result = $api->removeAllProducts();
  echo $result ? 'success' : 'failed', PHP_EOL;
  echo 'Products in shopping cart: ';
  dump($api->getShoppingCartProducts()->toArray());


  // Detail of order
  $orderId = 481;
  echo sprintf('Order #%d detail: ', $orderId);
  dump($api->getOrder($orderId)->toArray());


  // Lock order
  echo 'Locking order for adding next products to new order: ';
  $result = $api->lockOrder();
  echo $result ? 'success' : 'failed';


} catch (RequestException $exc) {
  echo sprintf(
    "\n>>> ERROR <<<\nCode: %d\nMessage: %s\nErrors:\n  %s",
    $exc->getCode(), trim($exc->getMessage()), implode("\n  ", $exc->getErrors() ?? [])
  );
  echo PHP_EOL;
} catch (\Throwable $exc) {
  dump($exc);
}

