<?php

namespace Vivantis\B2BApi\Exception;

class RequestException extends \Exception implements B2BApiException
{
  /** @var string[]|null */
  private ?array $errors;


  /**
   * @param string[]|null $errors
   */
  public function __construct(string $message, int $code, ?array $errors = null, ?\Throwable $previous = null)
  {
    parent::__construct($message, $code, $previous);
    $this->errors = $errors;
  }


  /**
   * @return string[]|null
   */
  public function getErrors(): ?array
  {
    return $this->errors;
  }
}
