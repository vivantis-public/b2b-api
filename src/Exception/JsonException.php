<?php

namespace Vivantis\B2BApi\Exception;

class JsonException extends \Exception implements B2BApiException
{
}