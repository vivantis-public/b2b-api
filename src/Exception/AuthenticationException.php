<?php

namespace Vivantis\B2BApi\Exception;

class AuthenticationException extends RuntimeException
{
}
