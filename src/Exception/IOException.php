<?php

namespace Vivantis\B2BApi\Exception;

class IOException extends RuntimeException
{
}