<?php

namespace Vivantis\B2BApi\Exception;

class RuntimeException extends \RuntimeException implements B2BApiException
{
}