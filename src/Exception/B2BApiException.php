<?php

namespace Vivantis\B2BApi\Exception;

interface B2BApiException extends \Throwable
{
}
