<?php

namespace Vivantis\B2BApi\Exception;

class EntityException extends \Exception implements B2BApiException
{
}
