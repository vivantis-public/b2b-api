<?php

namespace Vivantis\B2BApi\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements B2BApiException
{
}
