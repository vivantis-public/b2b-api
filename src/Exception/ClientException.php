<?php

namespace Vivantis\B2BApi\Exception;

class ClientException extends \Exception implements B2BApiException
{
}
