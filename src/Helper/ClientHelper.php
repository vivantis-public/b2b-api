<?php

namespace Vivantis\B2BApi\Helper;

use Psr\Http\Message\ResponseInterface;
use Vivantis\B2BApi\Exception\JsonException;
use function array_pop;
use function array_shift;
use function is_array;
use function lcfirst;
use function str_starts_with;
use function strtolower;

class ClientHelper
{
  const VivantisHeaderPrefix = 'X-Vivantis-';


  private function __construct() {}


  /**
   * @return array<int|string, mixed>|null
   * @throws JsonException
   */
  public static function dataFromBody(ResponseInterface $response): ?array
  {
    $body = $response->getBody()->getContents();

    return $body !== '' ? JsonHelper::decode($body) : null;
  }


  /**
   * @return array<string, string>
   */
  public static function getDataFromHeader(ResponseInterface $response): array
  {
    $prefix = strtolower(self::VivantisHeaderPrefix);
    $prefixLength = strlen($prefix);
    $headers = $response->getHeaders();

    $data = [];

    foreach ($headers as $name => $value) {
      if (!str_starts_with(strtolower($name), $prefix)) {
        continue;
      }

      $name = substr($name, $prefixLength);
      $name = lcfirst($name);
      $name = str_replace('-', '', $name);
      $data[$name] = array_shift($value);
    }

    return $data;
  }
}
