<?php

namespace Vivantis\B2BApi\Helper;

use Vivantis\B2BApi\Entity\Entity;
use Vivantis\B2BApi\Exception\EntityException;
use function explode;
use function str_ends_with;
use function str_replace;
use function ucfirst;

class EntityHelper
{
  private function __construct() {}


  public static function isRequestIndex(string $string): bool
  {
    return preg_match('~^[a-z][a-zA-Z]+$~', $string) === 1;
  }


  public static function isCollection(\ReflectionNamedType $type): bool
  {
    return str_ends_with($type->getName(), 'Collection');
  }


  /**
   * @throws EntityException
   */
  public static function valueAsType(mixed $value, \ReflectionNamedType $type): mixed
  {
    if (!isset($value) && $type->allowsNull()) {
      return null;
    }

    switch ($type->getName()) {
      case 'string':
        return (string)$value;
      case 'int':
        return (int)$value;
      case 'float':
        return (float)$value;
      case 'bool':
        return (bool)$value;
      case 'array':
        return (array)$value;
      default:
        throw new EntityException(sprintf('Cannot set unsupported type "%s".', $type->getName()));
    }
  }


  /**
   * @throws EntityException
   */
  public static function collectionForEntity(string $entityClass): string
  {
    $collectionClass = str_replace('Entity', 'Collection', $entityClass);
    $collectionClass = '\\' . trim($collectionClass, '\\');

    if (!class_exists($collectionClass)) {
      throw new EntityException(sprintf(
        'Entity "%s" not have collection, searched "%s".', $entityClass, $collectionClass
      ));
    }

    return $collectionClass;
  }


  /**
   * @throws EntityException
   */
  public static function entityForClient(string $clientClass): string
  {
    $entityClass = str_replace('Client', 'Entity', $clientClass);
    $entityClass = '\\' . trim($entityClass, '\\');

    if (!class_exists($entityClass)) {
      throw new EntityException(sprintf("Client '%s' not have entity, searched '%s'", $clientClass, $entityClass));
    }

    return $entityClass;
  }


  /**
   * @throws EntityException
   */
  public static function entityForCollection(string $clientClass): string
  {
    $entityClass = str_replace('Collection', 'Entity', $clientClass);
    $entityClass = '\\' . trim($entityClass, '\\');

    if (!class_exists($entityClass)) {
      throw new EntityException(sprintf('Collection "%s" not have entity, searched "%s".', $clientClass, $entityClass));
    }

    return $entityClass;
  }


  /**
   * @throws EntityException
   */
  public static function entityForRequestIndex(string $index): string
  {
    $namespace = implode('\\', explode('\\', Entity::class, -1));
    $namespace = '\\' . trim($namespace, '\\');

    $entityClass = ucfirst($index) . 'Entity';
    $entityClass = $namespace . '\\' . $entityClass;

    if (!class_exists($entityClass)) {
      throw new EntityException(sprintf("Request index '%s' not have entity, searched '%s'", $index, $entityClass));
    }

    return $entityClass;
  }
}
