<?php

namespace Vivantis\B2BApi\Helper;

use Vivantis\B2BApi\Exception\JsonException;
use function json_decode;
use function json_encode;
use const JSON_OBJECT_AS_ARRAY;
use const JSON_PRESERVE_ZERO_FRACTION;
use const JSON_THROW_ON_ERROR;
use const JSON_UNESCAPED_SLASHES;
use const JSON_UNESCAPED_UNICODE;

class JsonHelper
{
  private function __construct() {}


  /**
   * @param array<array-key, mixed> $data
   * @throws JsonException
   */
  public static function encode(array $data): string
  {
    try {
      return json_encode(
        $data,
        flags: JSON_THROW_ON_ERROR | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRESERVE_ZERO_FRACTION
      );
    } catch (\Throwable $exc) {
      throw new JsonException($exc->getMessage(), $exc->getCode(), $exc);
    }
  }


  /**
   * @return array<array-key, mixed>
   * @throws JsonException
   */
  public static function decode(string $json): array
  {
    try {
      return json_decode(
        $json,
        flags: JSON_THROW_ON_ERROR | JSON_OBJECT_AS_ARRAY
      );
    } catch (\Throwable $exc) {
      throw new JsonException($exc->getMessage(), $exc->getCode(), $exc);
    }
  }
}