<?php

namespace Vivantis\B2BApi\Helper;

use Vivantis\B2BApi\Exception\IOException;
use function chmod;
use function decoct;
use function dirname;
use function file_put_contents;
use function is_dir;
use function mkdir;
use function sprintf;

class IOHelper
{
  private function __construct() {}


  /**
   * @throws IOException
   */
  public static function write(string $file, string $content, ?int $mode = 0666): void
  {
    static::createDir(dirname($file));

    if (file_put_contents($file, $content) === false) {
      throw new IOException(sprintf("Unable to write file '%s'", $file));
    }

    if ($mode !== null && !chmod($file, $mode)) {
      throw new IOException(sprintf("Unable to chmod file '%s' to mode %s", $file, decoct($mode)));
    }
  }


  /**
   * @throws IOException
   */
  public static function createDir(string $dir, int $mode = 0777): void
  {
    if (
      !is_dir($dir) &&
      !@mkdir($dir, $mode, recursive: true) && // @ - non-atomic, dir may already exist
      !is_dir($dir)
    ) {
      throw new IOException(sprintf("Unable to create directory '%s' with mode %s", $dir, decoct($mode)));
    }
  }
}