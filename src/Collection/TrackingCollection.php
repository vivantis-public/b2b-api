<?php

namespace Vivantis\B2BApi\Collection;

use Vivantis\B2BApi\Entity\TrackingEntity;

/**
 * @extends Collection<TrackingEntity>
 */
class TrackingCollection extends Collection
{
}
