<?php

namespace Vivantis\B2BApi\Collection;

use Generator;
use Vivantis\B2BApi\Entity\Entity;
use Vivantis\B2BApi\Exception\EntityException;
use Vivantis\B2BApi\Exception\InvalidArgumentException;
use Vivantis\B2BApi\Helper\EntityHelper;
use function array_splice;
use function count;
use function is_int;


/**
 * @template T of Entity
 * @implements \IteratorAggregate<int, T>
 * @implements \ArrayAccess<int, T>
 */
abstract class Collection implements \ArrayAccess, \Countable, \IteratorAggregate
{
  /** @var T[] */
  private array $entities = [];


  /**
   * @param int $offset
   * @throws InvalidArgumentException
   */
  public function offsetExists($offset): bool
  {
    if (!is_int($offset)) {
      throw new InvalidArgumentException('Offset must be an integer');
    }

    return isset($this->entities[$offset]);
  }


  /**
   * @param int $offset
   * @return T
   * @throws InvalidArgumentException
   */
  public function offsetGet($offset): Entity
  {
    if (!is_int($offset) || $offset < 0 || $offset >= count($this->entities)) {
      throw new InvalidArgumentException('Offset invalid or out of range');
    }

    return $this->entities[$offset];
  }


  /**
   * @param int|null $offset
   * @param T $value
   * @throws InvalidArgumentException|EntityException
   */
  public function offsetSet($offset, $value): void
  {
    $entityClass = EntityHelper::entityForCollection($this::class);

    if (!($value instanceof $entityClass)) {
      throw new InvalidArgumentException(sprintf(
        "Value of collection must be instance of '%s', '%s' given",
        $entityClass, get_debug_type($value)
      ));
    }

    if ($offset === null) {
      $this->entities[] = $value;

    } elseif (!is_int($offset) || $offset < 0 || $offset >= count($this->entities)) {
      throw new InvalidArgumentException('Offset invalid or out of range');

    } else {
      $this->entities[$offset] = $value;
    }
  }


  /**
   * @param int $offset
   */
  public function offsetUnset($offset): void
  {
    if (!is_int($offset) || $offset < 0 || $offset >= count($this->entities)) {
      throw new InvalidArgumentException('Offset invalid or out of range');
    }

    array_splice($this->entities, $offset, 1);
  }


  public function count(): int
  {
    return count($this->entities);
  }


  /**
   * @return array<string, mixed>[]
   */
  public function toArray(): array
  {
    return array_map(static fn(Entity $entity) => $entity->toArray(), $this->entities);
  }


  /**
   * @return Generator<int, T>
   */
  public function getIterator(): Generator
  {
    foreach ($this->entities as $entity) {
      yield $entity;
    }
  }
}
