<?php

namespace Vivantis\B2BApi\Collection;

use Vivantis\B2BApi\Entity\OrderItemEntity;

/**
 * @extends Collection<OrderItemEntity>
 */
class OrderItemCollection extends Collection
{
}
