<?php

namespace Vivantis\B2BApi\Collection;

use Vivantis\B2BApi\Entity\ShoppingCartProductEntity;

/**
 * @extends Collection<ShoppingCartProductEntity>
 */
class ShoppingCartProductCollection extends Collection
{
}
