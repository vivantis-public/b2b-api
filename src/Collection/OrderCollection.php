<?php

namespace Vivantis\B2BApi\Collection;

use Vivantis\B2BApi\Entity\OrderEntity;

/**
 * @extends Collection<OrderEntity>
 */
class OrderCollection extends Collection
{
}
