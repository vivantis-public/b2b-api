<?php

namespace Vivantis\B2BApi\Collection;

use Vivantis\B2BApi\Entity\AddressEntity;

/**
 * @extends Collection<AddressEntity>
 */
class AddressCollection extends Collection
{
}
