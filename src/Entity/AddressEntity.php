<?php

namespace Vivantis\B2BApi\Entity;

class AddressEntity extends Entity
{
  public ?int $id = null;

  public string $name;

  public bool $default = false;

  public string $street;

  public ?string $numberD = null;

  public ?string $numberO = null;

  public string $city;

  public string $zip;

  public string $countryCode;

  public ?string $contactPerson;

  public ?string $phone;

  public ?string $email;
}