<?php

namespace Vivantis\B2BApi\Entity;

abstract class Entity
{
  /**
   * @param iterable<string, mixed>|null $values
   */
  public function __construct(?iterable $values = null)
  {
    if (isset($values)) {
      foreach ($values as $property => $value) {
        $this->{$property} = $value;
      }
    }
  }


  public function toArray(): array
  {
    return get_object_vars($this);
  }
}
