<?php

namespace Vivantis\B2BApi\Entity;

class ErrorEntity extends Entity
{
  public int $code;

  public string $status;

  public string $message;

  /** @var string[] */
  public array $errors = [];
}
