<?php

namespace Vivantis\B2BApi\Entity;

class OrderItemEntity extends Entity
{
  const STATE_ORDERED = 'ORDERED';
  const STATE_EXPEDITED = 'EXPEDITED';
  const STATE_CANCELED = 'CANCELED';
  const STATE_RELOCATED = 'RELOCATED';
  const STATE_DELAYED = 'DELAYED';
  const STATE_SPLITTED = 'SPLITTED';

  public string $productCode;

  public string $description;

  public int $quantity;

  public float $priceWithoutVat;

  public float $priceWithVat;

  public \DateTimeImmutable $inserted;

  public string $state;
}
