<?php

namespace Vivantis\B2BApi\Entity;

class OrderCreateEntity extends Entity
{
  public int $id;

  public ?string $description = null;

  public ?string $customerOrderNumber = null;
}
