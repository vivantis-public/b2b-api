<?php

namespace Vivantis\B2BApi\Entity;

use Vivantis\B2BApi\Collection\ShoppingCartProductCollection;

class ShoppingCartEntity extends Entity
{
  public string $priceLockedTo;

  /** @var ShoppingCartProductCollection<ShoppingCartProductEntity> */
  public ShoppingCartProductCollection $products;
}
