<?php

namespace Vivantis\B2BApi\Entity;

use Vivantis\B2BApi\Collection\OrderItemCollection;

class OrderEntity extends Entity
{
  const STATE_OPEN = 'OPEN';
  const STATE_LOCK = 'LOCK';
  const STATE_SHIPPING = 'SHIPPING';
  const STATE_PREPARED = 'PREPARED';
  const STATE_SHIPPED = 'SHIPPED';
  const STATE_CLOSED = 'CLOSED';


  public int $id;

  public \DateTimeImmutable $created;

  public string $state;

  public ?string $description;

  public ?string $customerOrderNumber;

  public bool $addingItems;

  public int $countItems;

  /** @var OrderItemCollection<OrderItemEntity> */
  public OrderItemCollection $items;
}
