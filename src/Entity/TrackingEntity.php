<?php

namespace Vivantis\B2BApi\Entity;

class TrackingEntity extends Entity
{
  public string $courier;
  
  public string $package;
  
  public string $trackingUrl;
}