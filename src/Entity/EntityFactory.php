<?php

namespace Vivantis\B2BApi\Entity;

use Vivantis\B2BApi\Collection\Collection;
use Vivantis\B2BApi\Exception\EntityException;
use Vivantis\B2BApi\Helper\EntityHelper;
use function assert;

class EntityFactory
{
  /** @var array<string, array<string, \ReflectionProperty>> */
  private static array $reflections = [];


  /**
   * @param array<string, mixed>|null $data
   * @throws EntityException
   */
  public static function createEntity(string $class, ?array $data = null): Entity
  {
    // class is index from request
    if (EntityHelper::isRequestIndex($class)) {
      $class = EntityHelper::entityForRequestIndex($class);
    }

    if (!class_exists($class)) {
      throw new EntityException(sprintf('Entity class "%s" not exists', $class));
    }

    if (!isset(self::$reflections[$class])) {
      self::$reflections[$class] = [];

      foreach ((new \ReflectionClass($class))->getProperties(\ReflectionProperty::IS_PUBLIC) as $refProperty) {
        self::$reflections[$class][$refProperty->getName()] = $refProperty;
      }
    }

    $entity = new $class;
    $props = self::$reflections[$class];
    foreach ($data as $name => $value) {
      // ignore value without property
      if (!isset($props[$name])) {
        continue;
      }

      $type = $props[$name]->getType();

      $entity->$name = $type instanceof \ReflectionNamedType
        ? self::getValue($type, $value)
        : $value;
    }

    return $entity;
  }


  /**
   * @param array<int, array<string, mixed>> $data
   * @throws EntityException
   */
  public static function createCollectionForClient(string $class, array $data): Collection
  {
    $entityClass = EntityHelper::entityForClient($class);
    $collection = self::createCollectionForEntity($entityClass);

    foreach ($data as $value) {
      $collection[] = new $entityClass($value);
    }

    return $collection;
  }


  /**
   * @throws EntityException
   */
  public static function createCollectionForEntity(string $entityClass): Collection
  {
    $class = EntityHelper::collectionForEntity($entityClass);

    return new $class;
  }


  private static function getValue(\ReflectionNamedType $type, mixed $value): mixed
  {
    if ($type->isBuiltin()) {
      return EntityHelper::valueAsType($value, $type);
    }

    $class = '\\' . $type->getName();

    if (EntityHelper::isCollection($type)) {
      $collection = new $class;
      assert($collection instanceof Collection);

      $entityClass = EntityHelper::entityForCollection($collection::class);

      foreach ($value as $item) {
        $collection[] = new $entityClass($item);
      }

      return $collection;
    }

    return new $class($value);
  }
}
