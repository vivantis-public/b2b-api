<?php

namespace Vivantis\B2BApi\Entity;

/**
 * @property-read string|null $token
 * @property-read string|null $state
 */
class HeaderEntity extends Entity
{
  /** @var array<string, mixed> */
  private array $headers = [];


  public function &__get(string $name): mixed
  {
    $value = $this->headers[$name] ?? null;

    return $value;
  }


  /**
   * @param array<string, int|string> $headers
   */
  public function __construct(array $headers)
  {
    parent::__construct();

    $this->headers = $headers;
  }


  /**
   * @param string[]|null $whiteList
   * @return array<string, mixed>
   */
  public function toArray(?array $whiteList = null): array
  {
    $arr = [];
    foreach ($this->headers as $name => $value) {
      if (isset($whiteList) && !in_array($name, $whiteList)) {
        continue;
      }
      $arr[$name] = $value;
    }

    return $arr;
  }
}
