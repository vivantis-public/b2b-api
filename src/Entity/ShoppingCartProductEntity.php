<?php

namespace Vivantis\B2BApi\Entity;

class ShoppingCartProductEntity extends Entity
{
  public string $productCode;

  public float $priceWithoutVat;

  public float $priceWithVat;

  public int $quantity;

  public int $inStock;

  public ?string $description;

  public bool $toOrder = true;
}
