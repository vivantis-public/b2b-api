<?php

namespace Vivantis\B2BApi\Http;

use Psr\Http\Message\StreamInterface;
use Vivantis\B2BApi\Exception\RuntimeException;

final class StringStream implements StreamInterface
{
  private ?string $content;

  private ?int $size;


  public function __construct(string $content)
  {
    $this->content = trim($content);
    $this->size = strlen($this->content);
  }


  public function __toString(): string
  {
    return $this->content ?? '';
  }


  public function getSize(): ?int
  {
    return $this->size;
  }


  public function getContents(): string
  {
    if (!isset($this->content)) {
      throw new RuntimeException('Stream is detached');
    }

    return $this->content;
  }


  public function getMetadata(?string $key = null): ?array
  {
    return isset($key) ? null : [];
  }


  public function close(): void
  {
    $this->content = null;
    $this->size = null;
  }


  public function detach()
  {
    if (!isset($this->content)) {
      return null;
    }

    $resource = fopen('php://temp', 'r+');
    fwrite($resource, $this->content);

    $this->content = $this->size = null;

    return $resource;
  }


  /**
   * @throws RuntimeException
   */
  public function tell(): int
  {
    throw new RuntimeException('Not implemented');
  }


  /**
   * @throws RuntimeException
   */
  public function eof(): bool
  {
    throw new RuntimeException('Not implemented');
  }


  public function isSeekable(): bool
  {
    return false;
  }


  /**
   * @throws RuntimeException
   */
  public function seek(int $offset, int $whence = SEEK_SET): void
  {
    throw new RuntimeException('Not seekable');
  }


  /**
   * @throws RuntimeException
   */
  public function rewind(): void
  {
    throw new RuntimeException('Not seekable');
  }


  public function isWritable(): bool
  {
    return false;
  }


  /**
   * @throws RuntimeException
   */
  public function write(string $string): int
  {
    throw new RuntimeException('Not writable');
  }


  public function isReadable(): bool
  {
    return true;
  }


  public function read(int $length): string
  {
    throw new RuntimeException('Not implemented');
  }
}
