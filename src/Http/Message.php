<?php

namespace Vivantis\B2BApi\Http;

use Psr\Http\Message\MessageInterface;
use Psr\Http\Message\StreamInterface;
use function dump;
use function implode;
use function strtolower;

class Message implements MessageInterface
{
  /** @var array<string, string[]> */
  private array $headers = [];

  private string $protocolVersion = '1.1';

  private ?StreamInterface $body = null;


  public function getProtocolVersion(): string
  {
    return $this->protocolVersion;
  }


  public function withProtocolVersion(string $version): self
  {
    if ($this->protocolVersion === $version) {
      return $this;
    }

    $clone = clone $this;
    $clone->protocolVersion = $version;

    return $clone;
  }


  /**
   * @return array<string, string[]>
   */
  public function getHeaders(): array
  {
    return $this->headers;
  }


  public function hasHeader(string $name): bool
  {
    $name = strtolower($name);

    return isset($this->headers[$name]);
  }


  public function getHeader(string $name): array
  {
    $name = strtolower($name);

    return $this->headers[$name] ?? [];
  }


  public function getHeaderLine($name): string
  {
    return implode(', ', $this->getHeader($name));
  }


  /**
   * @param string|string[] $value
   */
  public function withHeader(string $name, $value): self
  {
    $name = strtolower($name);

    $clone = clone $this;
    $clone->headers[$name] = (array)$value;

    return $clone;
  }


  public function withAddedHeader(string $name, $value): self
  {
    $clone = clone $this;

    if ($clone->hasHeader($name)) {
      $clone->headers[$name][] = $value;
    } else {
      $clone->headers[$name] = (array)$value;
    }

    return $clone;
  }


  public function withoutHeader(string $name): self
  {
    if (!$this->hasHeader($name)) {
      return $this;
    }

    $clone = clone $this;
    unset($clone->headers[$name]);

    return $clone;
  }


  public function getBody(): StreamInterface
  {
    return $this->body ??= new StringStream('');
  }


  public function withBody(StreamInterface $body): self
  {
    $clone = clone $this;
    $clone->body = $body;

    return $clone;
  }
}