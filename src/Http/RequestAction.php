<?php

namespace Vivantis\B2BApi\Http;

use Vivantis\B2BApi\Exception\InvalidArgumentException;
use function strtoupper;

enum RequestAction: string
{
  case Create = 'POST';
  case Read = 'GET';
  case Update = 'PUT';
  case Delete = 'DELETE';
  case Head = 'HEAD';


  /**
   * @throws InvalidArgumentException
   */
  public static function fromMethod(string $method): self
  {
    $method = strtoupper($method);

    return match ($method) {
      'POST' => self::Create,
      'GET' => self::Read,
      'PUT' => self::Update,
      'DELETE' => self::Delete,
      'HEAD' => self::Head,
      default => throw new InvalidArgumentException("Invalid HTTP request method, given '$method'"),
    };
  }
}