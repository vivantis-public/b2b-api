<?php
declare(strict_types = 1);

namespace Vivantis\B2BApi\Http;

use Psr\Http\Message\UriInterface;
use Vivantis\B2BApi\Exception\InvalidArgumentException;
use function count;
use function implode;
use function in_array;
use function rtrim;
use function sort;
use function sprintf;
use function strtolower;
use function trim;

final class Uri implements UriInterface
{
  private const ValidSchemes = ['http', 'https'];

  private string $scheme;

  private string $host;

  private string $path = '';

  private array $queries = [];

  private ?string $composed = null;


  /**
   * @throws InvalidArgumentException
   */
  public function __construct(string $hostWithScheme)
  {
    $this->applyHostWithScheme($hostWithScheme);
  }


  public function __toString(): string
  {
    return $this->composed ??= $this->compose();
  }


  public function __clone(): void
  {
    $this->composed = null;
  }


  public function getScheme(): string
  {
    return $this->scheme;
  }


  public function getAuthority(): string
  {
    return '';
  }


  public function getUserInfo(): string
  {
    return '';
  }


  public function getHost(): string
  {
    return $this->host;
  }


  public function getPort(): ?int
  {
    return null;
  }


  public function getPath(): string
  {
    return $this->path;
  }


  public function getQuery(): string
  {
    return http_build_query($this->queries, arg_separator: '&');
  }


  public function getFragment(): string
  {
    return '';
  }


  /**
   * @throws InvalidArgumentException
   */
  public function withScheme(string $scheme): self
  {
    throw new InvalidArgumentException('Not implemented');
  }


  /**
   * @throws InvalidArgumentException
   */
  public function withUserInfo(string $user, ?string $password = null): self
  {
    throw new InvalidArgumentException('Not implemented');
  }


  /**
   * @throws InvalidArgumentException
   */
  public function withHost(string $host): self
  {
    throw new InvalidArgumentException('Not implemented');
  }


  public function withPort(?int $port): self
  {
    // Not needs to be implemented
    return $this;
  }


  public function withPath(string $path): self
  {
    $path = trim($path);

    if ($path === '') {
      throw new InvalidArgumentException('Path cannot be empty');
    }

    if (!str_starts_with($path, '/')) {
      throw new InvalidArgumentException("Path must start with '/'");
    }

    if ($this->path === $path) {
      return $this;
    }

    $clone = clone $this;
    $clone->path = $path;

    return $clone;
  }


  /**
   * @param string|array<string, int|string> $query
   * @throws InvalidArgumentException
   */
  public function withQuery(string|array $query): self
  {
    if (is_string($query)) {
      throw new InvalidArgumentException('Query as string is not supported');
    }

    sort($query);
    sort($this->queries);

    if ($this->queries === $query) {
      return $this;
    }

    $clone = clone $this;
    $clone->queries = $query;

    return $clone;
  }


  public function withFragment(mixed $fragment): self
  {
    // Not needs to be implemented
    return clone $this;
  }


  /**
   * @throws InvalidArgumentException
   */
  private function applyHostWithScheme(string $hostWithScheme): void
  {
    $hostWithScheme = trim($hostWithScheme);
    $parts = explode(':', $hostWithScheme, 2);

    if (count($parts) !== 2) {
      throw new InvalidArgumentException("Host with scheme is invalid, given '$hostWithScheme'");
    }

    $scheme = strtolower($parts[0]);

    if (!in_array($scheme, self::ValidSchemes, strict: true)) {
      throw new InvalidArgumentException('Invalid scheme provided, expected ' . implode(' or ', self::ValidSchemes));
    }

    $host = trim($parts[1], '/');

    $this->scheme = $scheme;
    $this->host = $host;
  }


  private function compose(): string
  {
    $composed = sprintf(
      '%s://%s%s?%s',
      $this->getScheme(), $this->getHost(), $this->getPath(), $this->getQuery(),
    );

    return rtrim($composed, '?');
  }
}
