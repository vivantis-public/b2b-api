<?php

namespace Vivantis\B2BApi\Http;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\UriInterface;
use Vivantis\B2BApi\Exception\InvalidArgumentException;

final class Request extends Message implements RequestInterface
{
  public function __construct(
    private RequestAction $action,
    private UriInterface $uri,
  ) {}


  public function getRequestTarget(): string
  {
    return $this->uri->getPath() ?: '/';
  }


  public function getMethod(): string
  {
    return $this->action->value;
  }


  public function getUri(): UriInterface
  {
    return $this->uri;
  }


  public function withRequestTarget(string $requestTarget): RequestInterface
  {
    // Not needs to be implemented
    return clone $this;
  }


  /**
   * @throws InvalidArgumentException
   */
  public function withMethod(string $method): RequestInterface
  {
    $action = RequestAction::fromMethod($method);

    if ($this->action === $action) {
      return $this;
    }

    $clone = clone $this;
    $clone->action = $action;

    return $clone;
  }


  public function withUri(UriInterface $uri, $preserveHost = false): RequestInterface
  {
    $clone = clone $this;
    $clone->uri = $uri;

    return $clone;
  }
}
