<?php

namespace Vivantis\B2BApi\Client;

use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Vivantis\B2BApi\Collection\TrackingCollection;
use Vivantis\B2BApi\Exception\EntityException;
use Vivantis\B2BApi\Exception\JsonException;
use Vivantis\B2BApi\Exception\RequestException;
use Vivantis\B2BApi\Service\TokenService;

class TrackingClient extends Client
{
  public function __construct(ClientInterface $client, TokenService $tokenService)
  {
    parent::__construct('orders', $client, $tokenService);
  }


  /**
   * @throws RequestException|ClientExceptionInterface|EntityException|JsonException
   */
  public function forOrder(int $id): TrackingCollection
  {
    $tracking = $this->setRelation('tracking')
                     ->read($id);
    assert($tracking instanceof TrackingCollection);

    return $tracking;
  }
}