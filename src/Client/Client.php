<?php

namespace Vivantis\B2BApi\Client;

use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\ResponseInterface;
use Vivantis\B2BApi\Collection\Collection;
use Vivantis\B2BApi\Entity\Entity;
use Vivantis\B2BApi\Entity\EntityFactory;
use Vivantis\B2BApi\Entity\ErrorEntity;
use Vivantis\B2BApi\Entity\HeaderEntity;
use Vivantis\B2BApi\Exception\EntityException;
use Vivantis\B2BApi\Exception\InvalidArgumentException;
use Vivantis\B2BApi\Exception\JsonException;
use Vivantis\B2BApi\Exception\RequestException;
use Vivantis\B2BApi\Helper\ClientHelper;
use Vivantis\B2BApi\Helper\JsonHelper;
use Vivantis\B2BApi\Http\Request;
use Vivantis\B2BApi\Http\RequestAction;
use Vivantis\B2BApi\Http\StringStream;
use Vivantis\B2BApi\Http\Uri;
use Vivantis\B2BApi\Service\Authenticator;
use Vivantis\B2BApi\Service\TokenService;
use function array_is_list;
use function key;
use function sprintf;
use function trim;

abstract class Client
{
  private const HeaderTestMode = ClientHelper::VivantisHeaderPrefix . 'Test';
  #private const BaseHost = 'https://b2b.vivantis.cz';
  private const BaseHost = 'http://127.127.0.70:8080';
  private const BasePath = '/api/v2';

  private readonly Uri $uri;

  private readonly string $endpoint;

  private ?string $relation = null;

  private bool $testMode = false;


  public function __construct(
    string $endpoint,
    private readonly ClientInterface $client,
    private readonly TokenService $tokenService,
  ) {
    $this->uri = new Uri(self::BaseHost);
    $this->endpoint = $this->filterEndpoint($endpoint);
  }


  public function setTestMode(bool $testMode = true): void
  {
    $this->testMode = $testMode;
  }


  /**
   * @throws RequestException|ClientExceptionInterface|EntityException|JsonException
   */
  protected function create(Entity|Collection $entity): Entity|Collection
  {
    $uri = $this->createUri();
    $response = $this->request(RequestAction::Create, $uri, $entity);

    return $this->createEntity($response);
  }


  /**
   * @param array<string, mixed> $parameters
   * @throws RequestException|ClientExceptionInterface|EntityException|JsonException
   */
  protected function read(int|string|null $id = null, array $parameters = []): Entity|Collection
  {
    $uri = $this->createUri($id, $parameters);
    $response = $this->request(RequestAction::Read, $uri);

    return $this->createEntity($response);
  }


  /**
   * @throws RequestException|ClientExceptionInterface|JsonException
   */
  protected function readRaw(): string
  {
    $uri = $this->createUri();
    $response = $this->request(RequestAction::Read, $uri);

    return $response->getBody()->getContents();
  }


  /**
   * @throws RequestException|ClientExceptionInterface|EntityException|JsonException
   */
  protected function update(Entity|Collection $entity, int|string|null $id = null): Entity|Collection
  {
    $uri = $this->createUri($id);
    $response = $this->request(RequestAction::Update, $uri, $entity);

    return $this->createEntity($response);
  }


  /**
   * @throws RequestException|ClientExceptionInterface|JsonException
   */
  protected function delete(int|string|null $id = null): bool
  {
    $uri = $this->createUri($id);
    $response = $this->request(RequestAction::Delete, $uri);

    return $response->getStatusCode() === 204;
  }


  /**
   * @param array<string, mixed> $headers
   * @throws RequestException|ClientExceptionInterface|JsonException
   */
  protected function head(array $headers = []): HeaderEntity
  {
    $uri = $this->createUri();
    $response = $this->request(RequestAction::Head, $uri, headers: $headers);
    $data = ClientHelper::getDataFromHeader($response);

    return new HeaderEntity($data);
  }


  /**
   * @throws InvalidArgumentException
   */
  protected function setRelation(string $relation): self
  {
    $this->relation = $this->filterRelation($relation);

    return $this;
  }


  /**
   * @param array<string, int|string> $parameters
   */
  private function createUri(int|string|null $id = null, array $parameters = []): Uri
  {
    $path = sprintf('%s/%s', self::BasePath, $this->endpoint);

    if (isset($id)) {
      $path = sprintf('%s/%s', $path, $id);
    }

    if (isset($this->relation)) {
      $path = sprintf('%s/%s', $path, $this->relation);
      // relation is only for one use
      $this->relation = null;
    }

    return $this->uri->withPath($path)->withQuery($parameters);
  }


  /**
   * @throws EntityException|JsonException
   */
  private function createEntity(ResponseInterface $response): Entity|Collection|null
  {
    $data = ClientHelper::dataFromBody($response);

    if (!isset($data)) {
      return null;
    }

    if (array_is_list($data)) {
      $entity = EntityFactory::createCollectionForClient($this::class, $data);
    } else {
      $index = key($data);
      $entity = EntityFactory::createEntity($index, $data[$index]);
    }

    return $entity;
  }


  /**
   * @param array<string, mixed> $headers
   * @throws RequestException|ClientExceptionInterface|JsonException
   */
  private function request(
    RequestAction $action,
    Uri $uri,
    Entity|Collection|null $sendData = null,
    array $headers = [],
  ): ResponseInterface {
    $request = new Request($action, $uri);

    // create header with token for request
    $token = $this->tokenService->load();
    if (isset($token)) {
      $request = $request->withHeader(Authenticator::HeaderToken, $token);
    }

    // set testing mode
    if ($this->testMode) {
      $request = $request->withHeader(self::HeaderTestMode, '1');
    }

    foreach ($headers as $name => $value) {
      $request = $request->withHeader($name, $value);
    }

    // create data
    if (isset($sendData)) {
      $json = JsonHelper::encode($sendData->toArray());
      $request = $request->withBody(new StringStream($json));
    }

    $response = $this->client->sendRequest($request);

    if ($response->getStatusCode() >= 400) {
      $data = ClientHelper::dataFromBody($response) ?? ClientHelper::getDataFromHeader($response);
      $data['code'] ??= $response->getStatusCode();
      $errorEntity = new ErrorEntity($data);
      throw new RequestException($errorEntity->message, $errorEntity->code, $errorEntity->errors);
    }

    return $response;
  }


  /**
   * @throws InvalidArgumentException
   */
  private function filterEndpoint(string $endpoint): string
  {
    $endpoint = trim($endpoint);
    $endpoint = trim($endpoint, '/');

    if ($endpoint === '') {
      throw new InvalidArgumentException('Endpoint cannot be empty');
    }

    return $endpoint;
  }


  /**
   * @throws InvalidArgumentException
   */
  private function filterRelation(string $relation): string
  {
    $relation = trim($relation);
    $relation = trim($relation, '/');

    if ($relation === '') {
      throw new InvalidArgumentException('Relation cannot be empty');
    }

    return $relation;
  }
}
