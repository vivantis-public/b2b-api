<?php

namespace Vivantis\B2BApi\Client;

use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Vivantis\B2BApi\Exception\JsonException;
use Vivantis\B2BApi\Exception\RequestException;
use Vivantis\B2BApi\Service\Authenticator;
use Vivantis\B2BApi\Service\TokenService;

class AuthClient extends Client
{
  public function __construct(ClientInterface $client, TokenService $tokenService)
  {
    parent::__construct('auth', $client, $tokenService);
  }


  /**
   * @throws RequestException|ClientExceptionInterface|JsonException
   */
  public function login(string $secret): string
  {
    $entity = $this->setRelation('authorize')
                   ->head([Authenticator::HeaderAuth => $secret]);

    return $entity->token;
  }


  /**
   * @throws RequestException|ClientExceptionInterface|JsonException
   */
  public function verify(string $token): ?string
  {
    $entity = $this->setRelation('verify')
                   ->head([Authenticator::HeaderToken => $token]);

    return $entity->token;
  }
}
