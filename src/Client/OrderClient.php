<?php

namespace Vivantis\B2BApi\Client;

use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Vivantis\B2BApi\Collection\OrderCollection;
use Vivantis\B2BApi\Entity\OrderCreateEntity;
use Vivantis\B2BApi\Entity\OrderEntity;
use Vivantis\B2BApi\Exception\EntityException;
use Vivantis\B2BApi\Exception\InvalidArgumentException;
use Vivantis\B2BApi\Exception\JsonException;
use Vivantis\B2BApi\Exception\RequestException;
use Vivantis\B2BApi\Service\TokenService;

class OrderClient extends Client
{
  public function __construct(ClientInterface $client, TokenService $tokenService)
  {
    parent::__construct('orders', $client, $tokenService);
  }


  /**
   * @throws RequestException|ClientExceptionInterface|EntityException|JsonException
   */
  public function createOrder(OrderCreateEntity $orderCreateEntity): OrderEntity
  {
    $orderEntity = $this->create($orderCreateEntity);
    assert($orderEntity instanceof OrderEntity);

    return $orderEntity;
  }


  /**
   * @throws RequestException|ClientExceptionInterface|EntityException|JsonException
   */
  public function getOrders(?\DateTimeInterface $from = null, ?\DateTimeInterface $to = null): OrderCollection
  {
    $params = array_filter([
      'date_from' => $from?->format('Y-m-d'),
      'date_to' => $to?->format('Y-m-d'),
    ]);

    $orders = $this->read(parameters: $params);
    assert($orders instanceof OrderCollection);

    return $orders;
  }


  /**
   * @throws RequestException|ClientExceptionInterface|EntityException|JsonException
   */
  public function getOrder(int $id): ?OrderEntity
  {
    $order = $this->read($id);
    assert($order instanceof OrderEntity);

    if (!isset($order->id)) {
      return null;
    }

    return $order;
  }


  /**
   * @param string $extension One of 'pdf', 'xml', 'xlsx'
   * @throws RequestException|ClientExceptionInterface|InvalidArgumentException|JsonException
   */
  public function getOrderInvoice(int $id, string $extension): string
  {
    $allowed = ['pdf', 'xml', 'xlsx'];

    if (!in_array(strtolower($extension), $allowed, true)) {
      throw new InvalidArgumentException(sprintf(
        "Extension of invoice must be '%s', given '%s'",
        implode("' or '", $allowed), $extension
      ));
    }

    return $this->setRelation(sprintf('%d.%s', $id, $extension))
                ->readRaw();
  }


  /**
   * @throws RequestException|ClientExceptionInterface|JsonException
   */
  public function lockOrder(): bool
  {
    $this->setRelation('lock');
    $response = $this->head();

    return (bool)$response->state;
  }
}
