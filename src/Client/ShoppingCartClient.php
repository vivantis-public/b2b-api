<?php

namespace Vivantis\B2BApi\Client;

use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Vivantis\B2BApi\Collection\ShoppingCartProductCollection;
use Vivantis\B2BApi\Entity\ShoppingCartEntity;
use Vivantis\B2BApi\Entity\ShoppingCartProductEntity;
use Vivantis\B2BApi\Exception\EntityException;
use Vivantis\B2BApi\Exception\JsonException;
use Vivantis\B2BApi\Exception\RequestException;
use Vivantis\B2BApi\Service\TokenService;
use function strtolower;

class ShoppingCartClient extends Client
{
  public function __construct(ClientInterface $client, TokenService $tokenService)
  {
    parent::__construct('shopping-cart', $client, $tokenService);
  }


  /**
   * @throws RequestException|ClientExceptionInterface|EntityException|JsonException
   */
  public function getShoppingCart(): ShoppingCartEntity
  {
    $shoppingCart = $this->read();
    assert($shoppingCart instanceof ShoppingCartEntity);

    return $shoppingCart;
  }


  /**
   * @throws RequestException|ClientExceptionInterface|EntityException|JsonException
   */
  public function getProduct(string $productCode): ?ShoppingCartProductEntity
  {
    $productCode = strtolower($productCode);

    foreach ($this->getProducts() as $product) {
      assert($product instanceof ShoppingCartProductEntity);

      if (strtolower($product->productCode) === $productCode) {
        return $product;
      }
    }

    return null;
  }


  /**
   * @throws RequestException|ClientExceptionInterface|EntityException|JsonException
   */
  public function getProducts(): ShoppingCartProductCollection
  {
    $shoppingCart = $this->read();
    assert($shoppingCart instanceof ShoppingCartEntity);

    return $shoppingCart->products;
  }


  /**
   * @throws RequestException|ClientExceptionInterface|EntityException|JsonException
   */
  public function addProducts(ShoppingCartProductCollection $products): ShoppingCartEntity
  {
    $shoppingCart = $this->create($products);
    assert($shoppingCart instanceof ShoppingCartEntity);

    return $shoppingCart;
  }


  /**
   * @throws RequestException|ClientExceptionInterface|EntityException|JsonException
   */
  public function updateProducts(ShoppingCartProductCollection $products): ShoppingCartEntity
  {
    $shoppingCart = $this->create($products);
    assert($shoppingCart instanceof ShoppingCartEntity);

    return $shoppingCart;
  }


  /**
   * @throws RequestException|ClientExceptionInterface|JsonException
   */
  public function removeProduct(string $productCode): bool
  {
    return $this->delete($productCode);
  }


  /**
   * @throws RequestException|ClientExceptionInterface|JsonException
   */
  public function purge(): bool
  {
    return $this->delete();
  }


  /**
   * @throws RequestException|ClientExceptionInterface|JsonException
   */
  public function lockPrices(): bool
  {
    $response = $this->setRelation('lock-prices')
                     ->head();

    return (bool)$response->state;
  }
}
