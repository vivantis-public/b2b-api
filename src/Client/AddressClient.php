<?php

namespace Vivantis\B2BApi\Client;

use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Vivantis\B2BApi\Collection\AddressCollection;
use Vivantis\B2BApi\Entity\AddressEntity;
use Vivantis\B2BApi\Entity\AddressEntity;
use Vivantis\B2BApi\Exception\EntityException;
use Vivantis\B2BApi\Exception\JsonException;
use Vivantis\B2BApi\Exception\RequestException;
use Vivantis\B2BApi\Service\TokenService;

class AddressClient extends Client
{
  public function __construct(ClientInterface $client, TokenService $tokenService)
  {
    parent::__construct('addresses', $client, $tokenService);
  }


  /**
   * @throws RequestException|EntityException|ClientExceptionInterface|JsonException
   */
  public function createAddress(AddressEntity $addressEntity): AddressEntity
  {
    $addressEntity = $this->create($addressEntity);
    assert($addressEntity instanceof AddressEntity);

    return $addressEntity;
  }


  /**
   * @return AddressCollection
   * @throws RequestException|EntityException|ClientExceptionInterface|JsonException
   */
  public function getAddresses(): AddressCollection
  {
    $addresses = $this->read();
    assert($addresses instanceof AddressCollection);

    return $addresses;
  }
}
