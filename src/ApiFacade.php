<?php

namespace Vivantis\B2BApi;

use DateTimeImmutable;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\SimpleCache\CacheInterface;
use Vivantis\B2BApi\Client\AddressClient;
use Vivantis\B2BApi\Client\OrderClient;
use Vivantis\B2BApi\Client\ShoppingCartClient;
use Vivantis\B2BApi\Client\TrackingClient;
use Vivantis\B2BApi\Collection\AddressCollection;
use Vivantis\B2BApi\Collection\OrderCollection;
use Vivantis\B2BApi\Collection\ShoppingCartProductCollection;
use Vivantis\B2BApi\Collection\TrackingCollection;
use Vivantis\B2BApi\Entity\AddressEntity;
use Vivantis\B2BApi\Entity\OrderCreateEntity;
use Vivantis\B2BApi\Entity\OrderEntity;
use Vivantis\B2BApi\Entity\ShoppingCartEntity;
use Vivantis\B2BApi\Entity\ShoppingCartProductEntity;
use Vivantis\B2BApi\Exception\AuthenticationException;
use Vivantis\B2BApi\Exception\EntityException;
use Vivantis\B2BApi\Exception\IOException;
use Vivantis\B2BApi\Exception\JsonException;
use Vivantis\B2BApi\Exception\RequestException;
use Vivantis\B2BApi\Helper\IOHelper;
use Vivantis\B2BApi\Service\Authenticator;
use Vivantis\B2BApi\Service\TokenService;

class ApiFacade
{
  public readonly ShoppingCartClient $shoppingCartClient;

  public readonly AddressClient $addressClient;

  public readonly OrderClient $orderClient;

  public readonly TrackingClient $trackingClient;

  private readonly Authenticator $auth;


  public function __construct(
    private readonly int $clientId,
    private readonly string $clientSecretCode,
    ClientInterface $httpClient,
    CacheInterface $cache,
  ) {
    $tokenService = new TokenService($cache);

    $this->auth = new Authenticator($httpClient, $tokenService);
    $this->addressClient = new AddressClient($httpClient, $tokenService);
    $this->orderClient = new OrderClient($httpClient, $tokenService);
    $this->shoppingCartClient = new ShoppingCartClient($httpClient, $tokenService);
    $this->trackingClient = new TrackingClient($httpClient, $tokenService);
  }


  public function setTestMode(bool $testMode = true): void
  {
    $this->addressClient->setTestMode($testMode);
    $this->orderClient->setTestMode($testMode);
    $this->shoppingCartClient->setTestMode($testMode);
    $this->trackingClient->setTestMode($testMode);
  }


  /**
   * @throws AuthenticationException|ClientExceptionInterface|JsonException
   */
  public function init(): void
  {
    try {
      $this->auth->verify() ||
      $this->auth->authenticate($this->clientId, $this->clientSecretCode) ||
      throw new AuthenticationException('Cannot login');

    } catch (RequestException $e) {
      throw new AuthenticationException('Cannot login', previous: $e);
    }
  }


  /**
   * @throws AuthenticationException|RequestException|ClientExceptionInterface|EntityException|JsonException
   */
  public function getShoppingCartProducts(): ShoppingCartProductCollection
  {
    $this->init();

    return $this->shoppingCartClient->getProducts();
  }


  /**
   * @throws AuthenticationException|RequestException|ClientExceptionInterface|EntityException|JsonException
   */
  public function addProductToShoppingCart(
    string $productCode,
    int $quantity = 1,
    string $description = '',
    bool $toOrder = true,
  ): bool {
    $this->init();

    $actualProductEntity = $this->shoppingCartClient->getProduct($productCode);

    $productEntity = new ShoppingCartProductEntity;

    $productEntity->productCode = $productCode;
    $productEntity->quantity = $quantity;
    $productEntity->description = $description;
    $productEntity->toOrder = $toOrder;

    $collection = new ShoppingCartProductCollection;
    $collection[] = $productEntity;

    $cartEntity = $this->shoppingCartClient->addProducts($collection);

    if (isset($actualProductEntity)) {
      $quantity += $actualProductEntity->quantity;
    }

    $productCode = strtolower($productCode);

    foreach ($cartEntity->products as $product) {
      if (strtolower($product->productCode) === $productCode && $product->quantity >= $quantity) {
        return true;
      }
    }

    return false;
  }


  /**
   * @throws AuthenticationException|RequestException|ClientExceptionInterface|EntityException|JsonException
   */
  public function addProductsToShoppingCart(ShoppingCartProductCollection $collection): ShoppingCartEntity
  {
    $this->init();

    return $this->shoppingCartClient->addProducts($collection);
  }


  /**
   * @throws AuthenticationException|RequestException|ClientExceptionInterface|EntityException|JsonException
   */
  public function changeProductQuantity(string $productCode, int $quantity): bool
  {
    $this->init();

    $productEntity = new ShoppingCartProductEntity;

    $productEntity->productCode = $productCode;
    $productEntity->quantity = $quantity;

    $collection = new ShoppingCartProductCollection;
    $collection[] = $productEntity;

    $cartEntity = $this->shoppingCartClient->updateProducts($collection);

    $productCode = strtolower($productCode);

    foreach ($cartEntity->products as $product) {
      if (strtolower($product->productCode) === $productCode && $product->quantity === $quantity) {
        return true;
      }
    }

    return false;
  }


  /**
   * @throws AuthenticationException|RequestException|ClientExceptionInterface|EntityException|JsonException
   */
  public function changeProductDescription(string $productCode, string $description): bool
  {
    $this->init();

    $productCode = strtolower($productCode);

    $products = $this->getShoppingCartProducts();
    foreach ($products as $product) {
      if (strtolower($product->productCode) === $productCode) {
        break;
      }
    }

    if (isset($product)) {
      $product->description = $description;

      $collection = new ShoppingCartProductCollection;
      $collection[] = $product;

      $cartEntity = $this->shoppingCartClient->updateProducts($collection);
      foreach ($cartEntity->products as $product) {
        if (strtolower($product->productCode) === $productCode && $product->description === $description) {
          return true;
        }
      }
    }

    return false;
  }


  /**
   * @throws AuthenticationException|RequestException|ClientExceptionInterface|EntityException|JsonException
   */
  public function changeProductToOrder(string $productCode, bool $toOrder): bool
  {
    $this->init();

    $productCode = strtolower($productCode);

    $product = null;

    foreach ($this->getShoppingCartProducts() as $item) {
      if (strtolower($item->productCode) === $productCode) {
        $product = $item;
        break;
      }
    }

    if (!isset($product)) {
      return false;
    }

    $product->toOrder = $toOrder;

    $collection = new ShoppingCartProductCollection;
    $collection[] = $product;

    $cart = $this->shoppingCartClient->updateProducts($collection);

    foreach ($cart->products as $item) {
      if (strtolower($item->productCode) === $productCode && $item->toOrder === $toOrder) {
        return true;
      }
    }

    return false;
  }


  /**
   * @throws AuthenticationException|RequestException|ClientExceptionInterface|EntityException|JsonException
   */
  public function changeProductsInShoppingCart(ShoppingCartProductCollection $collection): ShoppingCartEntity
  {
    $this->init();

    return $this->shoppingCartClient->updateProducts($collection);
  }


  /**
   * @throws AuthenticationException|RequestException|ClientExceptionInterface|JsonException
   */
  public function removeProduct(string $productCode): bool
  {
    $this->init();

    return $this->shoppingCartClient->removeProduct($productCode);
  }


  /**
   * @throws AuthenticationException|RequestException|ClientExceptionInterface|JsonException
   */
  public function removeAllProducts(): bool
  {
    $this->init();

    return $this->shoppingCartClient->purge();
  }


  /**
   * @throws AuthenticationException|RequestException|ClientExceptionInterface|EntityException|JsonException
   * @throws \DateMalformedStringException
   */
  public function lockPrices(): ?DateTimeImmutable
  {
    $this->init();

    $result = $this->shoppingCartClient->lockPrices();

    if (!$result) {
      return null;
    }

    $cartEntity = $this->shoppingCartClient->getShoppingCart();

    return new DateTimeImmutable($cartEntity->priceLockedTo);
  }


  /**
   * @throws AuthenticationException|RequestException|ClientExceptionInterface|EntityException|JsonException
   */
  public function createAddress(AddressEntity $address): AddressEntity
  {
    $this->init();

    return $this->addressClient->createAddress($address);
  }


  /**
   * @throws AuthenticationException|RequestException|ClientExceptionInterface|EntityException|JsonException
   */
  public function getAddresses(): AddressCollection
  {
    $this->init();

    return $this->addressClient->getAddresses();
  }


  /**
   * @throws AuthenticationException|RequestException|ClientExceptionInterface|EntityException|JsonException
   */
  public function createOrder(
    AddressEntity $address,
    ?string $description = null,
    ?string $customerOrderNumber = null,
  ): OrderEntity {
    $this->init();

    $orderCreateEntity = new OrderCreateEntity;

    $orderCreateEntity->id = $address->id;
    $orderCreateEntity->description = $description;
    $orderCreateEntity->customerOrderNumber = $customerOrderNumber;

    return $this->orderClient->createOrder($orderCreateEntity);
  }


  /**
   * @throws AuthenticationException|RequestException|ClientExceptionInterface|EntityException|JsonException
   */
  public function getOrder(int $id): OrderEntity
  {
    $this->init();

    return $this->orderClient->getOrder($id);
  }


  /**
   * @throws AuthenticationException|RequestException|ClientExceptionInterface|EntityException|JsonException
   */
  public function getOrders(?\DateTimeInterface $from = null, ?\DateTimeInterface $to = null): OrderCollection
  {
    $this->init();

    return $this->orderClient->getOrders($from, $to);
  }


  /**
   * @throws AuthenticationException|RequestException|ClientExceptionInterface|EntityException|JsonException
   */
  public function lockOrder(): bool
  {
    $this->init();

    return $this->orderClient->lockOrder();
  }


  /**
   * @throws AuthenticationException|RequestException|ClientExceptionInterface|EntityException|JsonException
   */
  public function trackingOrder(int $id): TrackingCollection
  {
    $this->init();

    return $this->trackingClient->forOrder($id);
  }


  /**
   * @throws AuthenticationException|RequestException|ClientExceptionInterface|JsonException|IOException
   */
  public function saveInvoice(int $id, string $pathToFile): bool
  {
    $this->init();

    $file = new \SplFileInfo($pathToFile);
    $extension = $file->getExtension();

    $content = $this->orderClient->getOrderInvoice($id, $extension);

    IOHelper::write($file->getPathname(), $content);

    return true;
  }
}
