<?php

namespace Vivantis\B2BApi\Service;

use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Vivantis\B2BApi\Client\AuthClient;
use Vivantis\B2BApi\Exception\JsonException;
use Vivantis\B2BApi\Exception\RequestException;
use Vivantis\B2BApi\Helper\ClientHelper;

class Authenticator
{
  public const HeaderToken = ClientHelper::VivantisHeaderPrefix . 'Token';
  public const HeaderAuth = ClientHelper::VivantisHeaderPrefix . 'Auth';

  private AuthClient $authClient;


  public function __construct(
    ClientInterface $client,
    private readonly TokenService $tokenService,
  ) {
    $this->authClient = new AuthClient($client, $tokenService);
  }


  /**
   * @throws RequestException|ClientExceptionInterface|JsonException
   */
  public function authenticate(int $clientId, string $clientSecretCode): bool
  {
    $authValue = sprintf('%s$%s', $clientId, $clientSecretCode);

    $token = $this->authClient->login($authValue);
    $this->tokenService->save($token);

    return $token !== '';
  }


  /**
   * @throws ClientExceptionInterface|JsonException
   */
  public function verify(): bool
  {
    try {
      $token = $this->tokenService->load();

      if (empty($token)) {
        return false;
      }

      $token = $this->authClient->verify($token);
      $this->tokenService->save($token);

      return !empty($token);

    } catch (RequestException) {
      return false;
    }
  }
}
