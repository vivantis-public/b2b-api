<?php

namespace Vivantis\B2BApi\Service;

use Psr\SimpleCache\CacheInterface;

class TokenService
{
  private const StorageKey = 'vivantis_b2b_token';
  private const TokenExpire = 60 * 20;


  public function __construct(private readonly CacheInterface $cache) {}


  public function save(string $token): void
  {
    $this->cache->set(self::StorageKey, $token, self::TokenExpire);
  }


  public function load(): ?string
  {
    return $this->cache->get(self::StorageKey);
  }
}
